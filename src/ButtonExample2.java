import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.Cleaner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import org.w3c.dom.Text;



public class ButtonExample2 extends JFrame{
    JButton button;
    JTextField text;
    JButton clearbutton;
    public ButtonExample2(){
        super("Button Example");
        text = new JTextField();
        text.setBounds(40,50,100,40);
        button = new JButton("Click");
        button.setBounds(40, 100, 100, 40);
        button.setIcon(new ImageIcon("hello.png"));
        button.addActionListener((ActionListener) new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click Button"); 
                text.setText("Welcome to Programe");
            }
        });
        clearbutton = new JButton("Clear");
        clearbutton.setBounds(40,150,100,40);
        clearbutton.setIcon(new ImageIcon("clear.png"));
        clearbutton.addActionListener(new ActionListener (){

            @Override
            public void actionPerformed(ActionEvent e) {
               text.setText("");
                
            }

        });
        this.add(text);
        this.add(button);
        this.add(clearbutton);
        this.setSize(200,300);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        ButtonExample2 buttonExample2 = new ButtonExample2();
    }
}
