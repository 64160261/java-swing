import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HelloMyName extends JFrame{
    JLabel labelName;
    JTextField txtName;
    JButton btnHello;
    JLabel labelHello;


    public HelloMyName(){
        super("Hello My Name");
        labelName = new JLabel("Name: ");
        labelName.setBounds(10,20,100,20);
        

        txtName = new JTextField();
        txtName.setBounds(60,20,200,20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(40,50,250,20);
        btnHello.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                labelHello.setText("Hello "+ myName);
            }
        });

        labelHello = new JLabel("Hello My Name");
        labelHello.setHorizontalAlignment(JLabel.CENTER);
        labelHello.setBounds(30,70,250,20);

        this.add(labelHello);
        this.add(txtName);
        this.add(labelName);
        this.add(btnHello);

        this.setLayout(null);
        this.setSize(400,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
