import javax.swing.JButton;
import javax.swing.JFrame;



public class ButtonExample1 extends JFrame{
    JButton button;
    public ButtonExample1(){
        super("Button Example");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.add(button);
        this.setSize(200,300);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        ButtonExample1 buttonExample1 = new ButtonExample1();
    }
}
